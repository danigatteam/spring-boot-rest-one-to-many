# Example of Rest API CRUD with One to Many

- Code for 1:1 relation based on https://www.callicoder.com/hibernate-spring-boot-jpa-one-to-many-mapping-example/
- Code for REST service based on https://www.callicoder.com/spring-boot-rest-api-tutorial-with-mysql-jpa-hibernate/
- Code for enabling swagger based on http://www.baeldung.com/swagger-2-documentation-for-spring-rest-api

## URLs

- GET http://localhost:8080/api/students: Get all
- GET http://localhost:8080/api/students/{id}: Get one
- POST http://localhost:8080/api/students: Create
- DELETE http://localhost:8080/api/students/{id}

### Swagger

- http://localhost:8080/v2/api-docs: Raw json documentation
- http://localhost:8080/swagger-ui.html: The user interface

## To Do!

- DB Transaction

## Problems found

- GET http://localhost:8080/api/students returns the entities recursively
    *Solution:* add @JsonIgnore to Student.addresses

## Database

- Host is localhost, database is test (user test, no password)
- Tables are created automatically (spring.jpa.hibernate.ddl-auto = update)
- Log levels for hibernate so that we can debug all the SQL statements and learn what hibernate does under the hood.  
- How the relationship works:

  We use the mappedBy attribute in the Student entity to declare that it is not responsible for the relationship and hibernate should look for a field named student in the Address entity to find the configuration for the JoinColumn/ForeignKey Column.

