package com.privalia.restonetomany;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.privalia.restonetomany.model.Address;
import com.privalia.restonetomany.model.Student;
import com.privalia.restonetomany.repository.AddressRepository;
import com.privalia.restonetomany.repository.StudentRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootRestOneToManyApplicationTests {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    AddressRepository addressRepository;

    @Before
    public void tearDown() {
	addressRepository.deleteAllInBatch();
	studentRepository.deleteAllInBatch();
    }

    @Test
    public void createOneStudentWithTwoAddresses() {

	long initialStudents = studentRepository.count();
	long initialAddresses = addressRepository.count();

	Student student = new Student();
	student.setAge(12);
	student.setName("Test Name");

	Address address1 = new Address();
	address1.setAddress("C/Llull 113");
	address1.setZipcode("08005");
	address1.setStudent(student);

	Address address2 = new Address();
	address2.setAddress("C/Entença 45");
	address2.setZipcode("08014");
	address2.setStudent(student);

	student.getAddresses().add(address1);
	student.getAddresses().add(address2);

	studentRepository.save(student);

	assertEquals(initialStudents + 1, studentRepository.count());
	assertEquals(initialAddresses + 2, addressRepository.count());
    }

}
