package com.privalia.restonetomany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.privalia.restonetomany.model.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

}
