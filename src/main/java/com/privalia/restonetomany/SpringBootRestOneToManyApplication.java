package com.privalia.restonetomany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestOneToManyApplication {

    public static void main(String[] args) {
	SpringApplication.run(SpringBootRestOneToManyApplication.class, args);
    }
}
