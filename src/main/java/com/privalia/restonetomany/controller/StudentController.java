package com.privalia.restonetomany.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.privalia.restonetomany.model.Student;
import com.privalia.restonetomany.repository.StudentRepository;

@RestController
@RequestMapping("/api")
public class StudentController {

    @Autowired
    StudentRepository studentRepository;

    @GetMapping("/students")
    public List<Student> getAllStudents() {
	return studentRepository.findAll();
    }

    @PostMapping("/students")
    public ResponseEntity<Student> createStudent(@Valid @RequestBody Student student) {
	Student newStudent = studentRepository.save(student);
	return ResponseEntity.ok().body(newStudent);
    }

    @GetMapping("/students/{id}")
    public ResponseEntity<Student> getStudentById(@PathVariable(value = "id") Long studentId) {
	Student student = studentRepository.findOne(studentId);
	if (student == null) {
	    return ResponseEntity.notFound().build();
	}
	return ResponseEntity.ok().body(student);
    }

    @PutMapping("/students/{id}")
    public ResponseEntity<Student> updateStudent(@PathVariable(value = "id") Long studentId,
	    @Valid @RequestBody Student student) {
	Student existingStudent = studentRepository.findOne(studentId);

	if (null == existingStudent) {
	    return ResponseEntity.notFound().build();
	}

	existingStudent.setName(student.getName());
	existingStudent.setAge(student.getAge());

	Student updatedStudent = studentRepository.save(existingStudent);

	return ResponseEntity.ok(updatedStudent);
    }

    @DeleteMapping("/students/{id}")
    public ResponseEntity<Student> deleteStudent(@PathVariable(value = "id") Long studentId) {
	Student existingStudent = studentRepository.findOne(studentId);

	if (null == existingStudent) {
	    return ResponseEntity.notFound().build();
	}

	studentRepository.delete(studentId);
	return ResponseEntity.ok().build();
    }

}
